<?php

namespace App\Traits;

use App\Services\Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ConnectException;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Consume SRIKANDI
 */
trait ConsumeSrikandi
{
    public function performRequest($method, $requestUrl, $formParams = [])
    {
        try {
            $client = new Client([
                'base_uri' => config('srikandi.url'),
                'verify' => false,
                'headers' => [
                    'X-Requested-With' => 'XMLHttpRequest',
                    'Authorization' => 'Bearer ' . Auth::token(),
                    'User-Agent' => '17.8.19.45'
                ]
            ]);

            $response = $client->request($method, $requestUrl, [
                'form_params' => $formParams,
            ]);

            $result = $response->getBody()->getContents();
            $result = json_decode($result);

            if (!$result)
                return [
                    'type' => 'danger',
                    'success' => false,
                    'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                    'message' => 'Terjadi kesalahan pada server.',
                    'data' => null,
                ];

            $code = isset($result->code) ? $result->code : Response::HTTP_INTERNAL_SERVER_ERROR;
            if ($code != 200)
                throw new HttpException($code, isset($result->message) ? $result->message : $result->status);

            return [
                'type' => 'info',
                'success' => true,
                'code' => Response::HTTP_OK,
                'message' => isset($result->message) ? $result->message : '',
                'data' => isset($result->data) ? $result->data : null,
            ];
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $content = json_decode($response->getBody()->getContents());
            $code = $e->getCode() == 0 ? Response::HTTP_INTERNAL_SERVER_ERROR : $e->getCode();

            Log::error($code . ' | ' . $content);

            throw new HttpException($code, $content);
        } catch (ConnectException $e) {
            $code = $e->getCode() == 0 ? Response::HTTP_INTERNAL_SERVER_ERROR : $e->getCode();

            Log::error($code . ' | ' . $e->getMessage());

            throw new HttpException($code, $e->getHandlerContext()['error']);
        } catch (GuzzleException $e) {
            $code = $e->getCode() == 0 ? Response::HTTP_INTERNAL_SERVER_ERROR : $e->getCode();

            Log::error($code . ' | ' . $e->getMessage());

            throw new HttpException($code, $e->getMessage());
        }
    }
}
