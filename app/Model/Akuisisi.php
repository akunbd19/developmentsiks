<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Akuisisi extends Model
{
    protected $table = 'akuisisi';

    protected $fillable = [
        'berkas_id',
        'nama_berkas',
        'kurun',
        'status',
        'sifat',
        'status_arsip',
        'data',
    ];
}
