<?php

namespace App\Http\Controllers;
use App\Model\Akuisisi;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BAAkuisisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        
        $akuisisi = akuisisi::all();
        // return $akuisisi;

        return view('pages.Akuisisi.ba_akuisisi.index', compact('akuisisi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $data = (object)[
            'id' => $id,
            'nomor_berita_acara' => 'DI.01.02/1/2021',
            'tanggal_akuisisi' => Carbon::now()->addMonth(11)->addYear(1)->format('d M Y'),
            'pencipta_arsip' => 'Najwa Sihap',
            'nama_penerima' => 'Rangga Portugis',
            'total_berkas' => '3 Berkas',
            'total_arsip' => 2,
            
        ];

        $berkas = [
            (object)[
                'id' => rand(),
                'kode_klasifikasi' => 'LK.02',
                'desc' => 'D1.01.02/1/2021',
                'jra_aktif' => Carbon::now()->subMonth(2)->format('d M Y'),
                'tahun' => 'File Pendukung Dupak',
                'info_uraian_arsip' => 'Berkas Pengajuan DUPAK a.n Sahid Triambudi',
                'tahun' => '2021',
                'jumlah_arsip' => '2',
                'tanggal' => Carbon::now()->subMonth(2)->format('d M Y'),
            ],
            (object)[
                'id' => rand(),
                'kode_klasifikasi' => 'LK.02',
                'desc' => 'D1.01.02/1/2021',
                'jra_aktif' => Carbon::now()->subMonth(2)->format('d M Y'),
                'tahun' => 'File Pendukung Dupak',
                'info_uraian_arsip' => 'Berkas Pengajuan DUPAK a.n Sahid Triambudi',
                'tahun' => '2021',
                'jumlah_arsip' => '2',
                'tanggal' => Carbon::now()->subMonth(2)->format('d M Y'),
                
            ],
        ];

        return view('pages.Akuisisi.ba_akuisisi.show', compact(['data', 'berkas']));
    }
    public function detail($id)
    {
        $data = (object)[
            'id' => $id,
            'nomor_berita_acara' => 'DI.01.02/1/2021',
            'tanggal_akuisisi' => Carbon::now()->addMonth(11)->addYear(1)->format('d M Y'),
            'pencipta_arsip' => 'Najwa Sihap',
            'nama_penerima' => 'Rangga Portugis',
            'total_berkas' => '3 Berkas',
            'total_arsip' => 2,
            
        ];

        $berkas = [
            (object)[
                'id' => rand(),
                'jenis_naskah' => 'NOTA DINAS',
                'nomor_naskah' => 'DI.01.02/1/2021',
                'tanggal' => Carbon::now()->subMonth(2)->format('d M Y'),
                'uraian_arsip' => 'Uji Coba SRIKANDI Versi 2',
            ],
            (object)[
                'id' => rand(),
                'jenis_naskah' => 'DISPOSISI',
                'nomor_naskah' => 'DI.01.02/1/2021',
                'tanggal' => Carbon::now()->subMonth(2)->format('d M Y'),
                'uraian_arsip' => 'Uji Coba SRIKANDI Versi 2',
                
                
            ],
        ];
        return view('pages.Akuisisi.ba_akuisisi.detail', compact(['data', 'berkas']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
