<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Traits\ConsumeSrikandi;

class UsulSerahController extends Controller
{
    use ConsumeSrikandi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $response = $this->performRequest('GET', '/api/berkas');
        
        // dd($response['data']);
        return view('pages.akuisisi.usul_serah.index', [
            'data' => $response['data']
        ]);
    }

    public function get(Request $request)
    {
        $response = $this->performRequest('GET', '/api/berkas', $request->all());

        return response()->json(['data' => $response['data']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $response = $this->performRequest('GET', '/api/berkas/' . $id . '/show');

        // dd(Arr::get($response, 'data')->item_berkas);

        return view('pages.akuisisi.usul_serah.show', [
            'data' => Arr::get($response, 'data')->berkas,
            'berkas' => Arr::get($response, 'data')->item_berkas,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
