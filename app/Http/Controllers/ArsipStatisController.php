<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class ArsipStatisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            (object)[
                'id' => rand(),
                'kode_referensi' => 'IDN-RAB.09.06-15032-XXX-YYY-01',
                'kode_klasifikasi' => 'LK.02',
                'informasi_uraian_arsip' => 'Berkas Pengajuan DUPAK a.n Sahid Triambudi',
                'jumlah_arsip' => '2',
                'akhir_retensi_arsip' => Carbon::now()->subMonth(2)->format('d M Y'),
            ],
            (object)[
                'id' => rand(),
                'kode_referensi' => '',
                'kode_klasifikasi' => 'LK.02',
                'informasi_uraian_arsip' => 'Berkas Pengajuan DUPAK a.n Sahid Triambudi',
                'jumlah_arsip' => '2',
                'akhir_retensi_arsip' => Carbon::now()->subMonth(2)->format('d M Y'),
                
            ],
        ];

        return view('pages.pengolahan.arsip_statis.index', compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
