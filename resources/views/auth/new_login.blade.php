@extends('layouts.begin_auth')

@section('title', 'Masuk')

@section('content')
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content d-flex justify-content-center align-items-center">

            <!-- Login form -->
            <form class="login-form" action="{{ route('signin') }}" method="POST">
                @csrf
                <div class="card mb-0">
                    <div class="card-body">
                        <div class="text-center mb-3">
                            <img src="{{ asset('images/logo-anri.png') }}" width="150" height="100%">
                            <h5 class="mb-0">Masuk ke akun Anda</h5>
                            <span class="d-block text-muted">Masukkan kredensial Anda di bawah ini</span>
                        </div>
                         @include('messages.alert')
                        <div class="form-group form-group-feedback form-group-feedback-left">
                            <input type="text" class="form-control" name="username" placeholder="Nama Pengguna">
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                            @error('username')
                            <span class="text-danger">
                                {{ $message }}
                            </span>
                            @enderror
                        </div>
                        <div class="form-group form-group-feedback form-group-feedback-left">
                            <input type="password" class="form-control" name="password" placeholder="Kata Sandi">
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                            @error('password')
                            <span class="text-danger">
                                {{ $message }}
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Masuk <i
                                    class="icon-circle-right2 ml-2"></i></button>
                        </div>
                        {{-- <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="{{ asset('images/lembaga-support-2.png') }}" width="100%">
                            </div>
                        </div>
                        <span class="form-text text-center text-muted">Copyright &copy; Pemerintah Indonesia</span>
                        <div class="row mt-2">
                            <div class="col-sm-12 text-center">
                                <img src="{{ asset('images/bsre.png') }}" width="30%" class="justify-content-center">
                            </div>
                        </div> --}}
                    </div>
                </div>
            </form>
            <!-- /login form -->

        </div>
        <!-- /content area -->


        <!-- Footer -->
        {{-- @include('include.footer') --}}
        <!-- /footer -->

    </div>
    <!-- /main content -->

</div>
@endsection
