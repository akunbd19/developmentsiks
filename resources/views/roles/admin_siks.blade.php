<li class="nav-item nav-item-submenu @yield('open_administrasi')">
    <a href="#" class="nav-link @yield('administrasi')"><i class="icon-office"></i> <span>administrasi</span></a>
    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        {{-- <li class="nav-item">
            <a href="#" class="nav-link @yield('organisasi')">Unit Kerja</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link @yield('jabatan')">Jabatan</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link @yield('pengguna')">Pengguna</a>
        </li> --}}

        
        <li class="nav-item">
            <a href="{{ route('unit-kerja.index') }}" class="nav-link @yield('Unit Kerja')">

                <span>
                    Unit Kerja
                </span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('jabatan.index') }}" class="nav-link @yield('Jabatan')">

                <span>
                    Jabatan
                </span>
            </a>
        </li>
        
        <li class="nav-item">
            <a href="{{ route('pengguna.index') }}" class="nav-link @yield('Pengguna')">

                <span>
                    Pengguna
                </span>
            </a>
        </li>
        </ul>
</li>

