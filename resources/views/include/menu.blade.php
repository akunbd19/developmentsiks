<ul class="nav nav-sidebar" data-nav-type="accordion">

    <!-- Main -->
    <li class="nav-item-header">
        {{-- <div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i> --}}
    </li>
    <li class="nav-item">
        <a href="{{ route('dashboard.index') }}" class="nav-link @yield('dashboard')">
            <i class="icon-home4"></i>
            <span>
                Beranda
            </span>
        </a>
    </li>
    @role('Admin SIKS')
    @include('roles.admin_siks')
    @include('roles.akusisi')
    @include('roles.pengolahan')
    @endrole
    @role('Admin Nasional')
    @include('roles.admin_siks')
    @endrole
    @role('Akusisi')
    @include('roles.akusisi')
    @endrole
</ul>
