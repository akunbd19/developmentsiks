@extends('layouts.begin_back')

@section('open_administrasi', 'nav-item-expanded nav-item-open')

@section('administrasi', 'active')

@section('organisasi', 'active')

@section('title', 'Buat Unit Kerja / Satker')

@section('content')
    <!-- Page header -->
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>
                <i class="icon-city mr-2"></i> <span class="font-weight-semibold">Unit Kerja / Satker</span> - Buat Baru
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route('unit-kerja.index') }}" class="btn bg-grey btn-labeled btn-labeled-left"><b><i
                            class="icon-arrow-left8"></i></b>Kembali</a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <div class="page-content pt-0">
        <div class="content-wrapper">
            <div class="content">

                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title"> Form Unit Kerja / Satker Baru</h5>
                            </div>

                            <form action="{{route('unit-kerja.store')}}" method="POST">
                                @csrf
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Induknya <span class="text-danger">*</span></label>
                                                <select name="parent_id"
                                                        id="parent_id"
                                                        class="form-control select2"
                                                        data-placeholder="Pilih Instansi / Unit Kerja ..."
                                                        required>
                                                    <option></option>
                                                    @foreach($organisasi as $item)
                                                        <option value="{{$item->id}}">{{$item->nama}}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('parent_id'))
                                                    <label id="basic-error"
                                                           class="validation-invalid-label"
                                                           for="basic">
                                                        {{$errors->first('parent_id')}}
                                                    </label>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Nama Unit kerja <span class="text-danger">*</span></label>
                                                <input type="text"
                                                       name="nama"
                                                       id="nama"
                                                       class="form-control"
                                                       placeholder="Masukkan nama Unit kerja ..."
                                                       required>
                                                @if($errors->has('nama'))
                                                    <label id="basic-error" class="validation-invalid-label"
                                                           for="basic">
                                                        {{$errors->first('nama')}}
                                                    </label>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Singkatan</label>
                                                <input type="text"
                                                       name="singkatan"
                                                       id="singkatan"
                                                       class="form-control"
                                                       placeholder="Masukkan singkatan...">
                                            </div>
                                            <div class="form-group">
                                                <label>Kode</label>
                                                <input type="text"
                                                       name="kode"
                                                       class="form-control"
                                                       placeholder="Masukkan kode...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Alamat</label>
                                                <textarea name="alamat"
                                                          rows="5"
                                                          id="alamat"
                                                          class="form-control"
                                                          placeholder="Masukkan alamat..."></textarea>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">Latitude</label>
                                                        <input type="text"
                                                               name="lat"
                                                               id="lat"
                                                               class="form-control"
                                                               placeholder="Masukkan latitude...">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">Longitude</label>
                                                        <input type="text"
                                                               name="long"
                                                               id="long"
                                                               class="form-control"
                                                               placeholder="Masukkan longtitude...">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    <button type="submit" class="btn btn-success btn-labeled btn-labeled-left">
                                        <b><i class="icon-floppy-disk"></i></b> Simpan
                                    </button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('addon-script')
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
@endpush

@section('footer-script')
    <script>
        $(document).ready(function () {
            $('.select2').select2();
        });
    </script>
@endsection
