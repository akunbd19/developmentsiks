@extends('layouts.begin_back')

@section('open_administrasi', 'nav-item-expanded nav-item-open')

@section('administrasi', 'active')

@section('organisasi', 'active')

@section('title', 'Edit Pengguna')

@section('content')
    <!-- Page header -->
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>
                <i class="icon-city mr-2"></i> <span class="font-weight-semibold">Pengguna</span> - Edit
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route('pengguna.index') }}" class="btn bg-grey btn-labeled btn-labeled-left"><b><i
                            class="icon-arrow-left8"></i></b>Kembali</a>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <div class="page-content pt-0">
        <div class="content-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title"> Form Edit Pengguna </h5>
                            </div>
                            <form action="{{ route('pengguna.update', $user->id) }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="nomor_induk">Nomor Induk Pegawai <span
                                                class="text-danger">*</span></label>
                                        <input type="text" name="nomor_induk" class="form-control"
                                            placeholder="Masukkan Nomor Induk.." value="{{ $user->nomor_induk }}">
                                        @error('nomor_induk')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="NIK">NIK <span class="text-danger">*</span></label>
                                        <input type="text" name="nik" class="form-control" placeholder="Masukkan NIK.."
                                            value="{{ $user->nik }}">
                                        @error('nik')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="Nama">Nama Lengkap <span class="text-danger">*</span></label>
                                        <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama..."
                                            value="{{ $user->nama }}">
                                        @error('nama')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="telepon">Nomor Seluler</label>
                                        <input type="text" name="telepon" class="form-control"
                                            placeholder="Masukkan Nomor Seluler..." value="{{ $user->telepon }}">
                                        @error('telepon')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Nama Pengguna <span class="text-danger">*</span></label>
                                        <input type="text" name="username" class="form-control"
                                            placeholder="Masukkan Nama Pengguna..." value="{{ $user->username }}">
                                        @error('username')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email <span class="text-danger">*</span></label>
                                        <input type="text" name="email" class="form-control" placeholder="Masukkan Email..."
                                            value="{{ $user->email }}">
                                        @error('email')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="roles">Hak Akses <span class="text-danger">*</span></label>
                                        <select name="roles" class="form-control select2" data-placeholder="Pilih Hak Akses"
                                            style="width:100%;" id="roles" onchange="selectedRoles()">
                                            <option></option>
                                            {{-- @foreach($roles as $item)
                                            <option value="{{ $item->name }}"
                                                {{ $item->name == $user->getRoleNames()[0] ? 'selected' : '' }}>
                                                {{ $item->name }}</option>
                                            @endforeach --}}
                                        </select>
                                        @error('roles')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    {{-- <div class="form-group" id="check-role">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" value="YES" name="send_outside"
                                                {{ $user->send_outside == "YES" ? 'checked' : '' }}
                                                class="custom-control-input" id="custom-check-box">
                                            <label class="custom-control-label" for="custom-check-box">
                                                <div id="check-user">
                                                    <b>User</b> ini dapat mengirim dan atau menerima Surat dari Luar
                                                    Instansi / Unit Kerja / Satuan Kerja?
                                                </div>
                                                <div id="check-pencatat-surat">
                                                    Pengguna ini menjadi <b>Pencatat Surat Utama</b>?
                                                </div>
                                                <div id="check-unit-kearsipan">
                                                    Pengguna ini menjadi <b>Pengelola retensi diatas 10 tahun</b>?
                                                </div>
                                            </label>
                                        </div>
                                    </div> --}}
                                    <div class="form-group">
                                        <label for="organisasi_id">Instansi / Unit Kerja <span
                                                class="text-danger">*</span></label>
                                        <select name="organisasi_id" id="organisasi_id" class="form-control select2"
                                            data-placeholder="Pilih Instansi / Unit Kerja..." style="width:100%;">
                                            <option></option>
                                            {{-- @foreach($organisasi as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $item->id == $user->organisasi_id ? 'selected' : '' }}>
                                                {{ $item->nama }}</option>
                                            @endforeach --}}
                                        </select>
                                        @error('organisasi_id')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="jabatan_id">Jabatan <span class="text-danger">*</span></label>
                                        <select name="jabatan_id" id="jabatan_id" class="form-control select2"
                                            data-placeholder="Pilih Jabatan" style="width:100%;">
                                            <option></option>
                                            {{-- @foreach($jabatan as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $item->id == $user->jabatan_id ? 'selected' : '' }}>
                                                {{ $item->nama }}</option>
                                            @endforeach --}}
                                        </select>
                                        @error('jabatan_id')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="grup_jabatan_id">Grup Jabatan</label>
                                        <select name="grup_jabatan_id" id="grup_jabatan_id" class="form-control select2"
                                            data-placeholder="Pilih Jabatan" style="width:100%;">
                                            <option></option>
                                            {{-- @foreach($grupJabatan as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $item->id == $user->grup_jabatan_id ? 'selected' : '' }}>
                                                {{ $item->nama }}</option>
                                            @endforeach --}}
                                        </select>
                                        @error('grup_jabatan_id')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="tipe_pengguna_id">Jenis Pengguna</label>
                                        <select name="tipe_pengguna_id" id="tipe_pengguna_id" class="form-control select2"
                                            data-placeholder="Pilih Jenis Pengguna" style="width:100%;">
                                            <option></option>
                                            {{-- @foreach($tipePengguna as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $item->id == $user->tipe_pengguna_id ? 'selected' : '' }}>
                                                {{ $item->nama }}</option>
                                            @endforeach --}}
                                        </select>
                                        @error('tipe_pengguna_id')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-3 mb-md-2">
                                        <label class="d-block">Status</label>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input-styled" name="status"
                                                    value="AKTIF" {{ $user->status == 'AKTIF' ? 'checked' : '' }} data-fouc>
                                                <span class="badge badge-success">AKTIF</span>
                                            </label>
                                        </div>
    
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input-styled" name="status"
                                                    value="NON-AKTIF" {{ $user->status == 'NON-AKTIF' ? 'checked' : '' }}
                                                    data-fouc>
                                                <span class="badge badge-secondary">NON-AKTIF</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Foto</label>
                                        <div class="media mt-0">
                                            <div class="mr-3">
                                                <a href="#">
                                                    <img src="{{ $user->avatar_link }}" width="60" height="60"
                                                        class="rounded-round" alt="" id="previewAvatar"
                                                        style="object-fit: cover; object-position: 50% 0%;">
                                                </a>
                                            </div>
    
                                            <div class="media-body">
                                                <input type="file" name="file" class="form-input-styled" id="avatar"
                                                    data-fouc>
                                                <span class="form-text text-muted">Format yang didukung: .jpg .jpeg
                                                    .png</span>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="form-group" id="check-notifikasi">
                                        <label for="notification">Notifikasi</label>
                                        <div class="form-check form-check-switchery">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input-switchery" name="notif_wa"
                                                    value="true" {{ $user->notif_wa == true ? 'checked':'' }} data-fouc>
                                                Terima notifikasi <i><b>Whatsapp</b></i> dari SIKS
                                            </label>
                                        </div>
                                        <div class="form-check form-check-switchery">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input-switchery" name="notif_email"
                                                    value="true" {{ $user->notif_email == true ? 'checked':'' }} data-fouc>
                                                Terima notifikasi <i><b>Email</b></i> dari SIKS
                                            </label>
                                        </div>
                                    </div> --}}
                                </div>
                                <div class="card-footer text-right">
                                    <button type="submit" class="btn btn-primary btn-labeled btn-labeled-left"><b><i
                                                class="icon-floppy-disk"></i></b> Perbarui</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title"> Form Ganti Kata Sandi </h5>
                            </div>
                            <form action="{{ route('user.password') }}" method="post">
                                @csrf
                                <input type="hidden" name="user_id" value="{{ $user->id }}" readonly>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="password">Kata Sandi Baru <span class="text-danger">*</span></label>
                                        <input type="password" name="password" class="form-control"
                                            placeholder="Minimal 8 karakter..." value="{{ old('password') }}">
                                        @error('password')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="password_confirmation">Konfirmasi Kata Sandi Baru <span
                                                class="text-danger">*</span></label>
                                        <input type="password" name="password_confirmation" class="form-control"
                                            placeholder="Ketik ulang kata sandi..."
                                            value="{{ old('password_confirmation') }}">
                                        @error('password_confirmation')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    <button type="submit" class="btn btn-warning btn-labeled btn-labeled-left"><b><i
                                                class="fas fa-lock"></i></b> Ganti</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    
    @push('addon-script')
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    @endpush
    
    @section('footer-script')
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
    <script>
        var type = $("#roles").val();
    
        if (type == 'User') {
            $('#check-role').show();
            $("#check-user").show();
            $("#check-notifikasi").show();
    
            $("#check-pencatat-surat").hide();
            $("#check-unit-kearsipan").hide();
        } else if (type == 'Tata Usaha / Sekretaris') {
            $('#check-role').show();
            $("#check-pencatat-surat").show();
    
            $("#check-user").hide();
            $("#check-notifikasi").hide();
            $("#check-unit-kearsipan").hide();
        } else if (type == 'Unit Kearsipan') {
            $('#check-role').show();
            $("#check-unit-kearsipan").show();
    
            $("#check-pencatat-surat").hide();
            $("#check-user").hide();
            $("#check-notifikasi").hide();
        } else {
            $('#check-role').hide();
            $("#check-user").hide();
            $("#check-notifikasi").hide();
            $("#check-pencatat-surat").hide();
            $("#check-unit-kearsipan").hide();
        }
    
        function selectedRoles() {
            var type = $("#roles").val();
    
            if (type == 'User') {
                $('#check-role').show();
                $("#check-user").show();
                $("#check-notifikasi").show();
    
    
                $("#check-pencatat-surat").hide();
                $("#check-unit-kearsipan").hide();
            } else if (type == 'Tata Usaha / Sekretaris') {
                $('#check-role').show();
                $("#check-pencatat-surat").show();
    
                $("#check-unit-kearsipan").hide();
                $("#check-user").hide();
                $("#check-notifikasi").hide();
            } else if (type == 'Unit Kearsipan') {
                $('#check-role').show();
                $("#check-unit-kearsipan").show();
    
                $("#check-pencatat-surat").hide();
                $("#check-user").hide();
                $("#check-notifikasi").hide();
            } else {
                $('#check-role').hide();
                $("#check-user").hide();
                $("#check-notifikasi").hide();
                $("#check-unit-kearsipan").hide();
                $("#check-pencatat-surat").hide();
            }
        }
    
        $(document).ready(function () {
            $('.select2').select2({
                allowClear: true
            });
    
            //preview avatar
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#previewAvatar').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
    
            $("#avatar").change(function () {
                readURL(this);
            });
    
            $('#organisasi_id').change(function () {
                var id = $(this).val();
    
                $.ajax({
                        url: "{{ route('get-data.jabatan') }}?organisasi_id=" + id,
                        type: 'GET',
                        dataType: 'json',
                    })
                    .done(function (response) {
                        if (response.status == true) {
                            $('#jabatan_id').empty();
                            $('#jabatan_id').append('<option></option>');
    
                            $.each(response.data, function (key, value) {
                                $('#jabatan_id').append('<option value="' + value.id + '">' +
                                    value
                                    .nama + '</option>')
                            });
                        } else if (response.status == false) {
                            $('#jabatan_id').empty();
                            $('#jabatan_id').append('<option></option>');
                        }
                    });
            });
        });
    
    </script>
    @endsection