@extends('layouts.begin_back')

@section('open_akuisisi', 'nav-item-expanded nav-item-open')

@section('akuisisi', 'active')

@section('akuisisi_index', 'active')

@section('title', 'Akuisisi Arsip')

@section('styles')
    <link rel="stylesheet" href="{{ asset('global_assets/plugins/sweetalert2/dist/sweetalert2.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('global_assets/plugins/gyrocode-datatables-checkboxes/css/dataTables.checkboxes.css') }}">

    <style>
        .nowrap {
            white-space: nowrap;
        }
    </style>
@endsection

@section('content')
    <!-- Page header -->
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>
                <i class="icon-drawer-in mr-2"></i> <span class="font-weight-semibold">Akuisisi Arsip</span> - List
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
            <a href="#"
               class="btn bg-indigo-400 btn-labeled btn-labeled-left d-none" id="btn-akuisisi-arsip">
                <b><i class="icon-folder-open3"></i></b> Akuisisi Arsip
            </a>
        </div>
    </div>
    <!-- /page header -->


    <div class="page-content pt-0">
        <div class="content-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-header bg-white">
                                <h6 class="card-title">Akuisisi Arsip</h6>
                            </div>
                            <table class="table datatable-responsive-control table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>No</th>
                                    <th style="width: 20%">Kode Klasifikasi</th>
                                    <th style="width: 60%">Uraian Arsip</th>
                                    <th>Tahun</th>
                                    <th>Jumlah Arsip</th>
                                    <th style="width: 40%">Akhir Retensi Arsip</th>
                                    <th style="width: 20%; white-space: nowrap;" class="text-center">Aksi</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--@foreach ([] as $key => $item)
                                    <tr>
                                        <td></td>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->klasifikasi->kode }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ \Carbon\Carbon::parse($item->created_at)->format('Y') }}</td>
                                        <td>{{ count($item->berkas_kategori) }}</td>
                                        <td>{{ $item->kurun }}</td>
                                        <td style="white-space: nowrap">
                                            <a href="{{ route('akuisisi.show', ['id' => $item->id]) }}"
                                               title="Lihat Detail"
                                               class="list-icons-item text-info btn-icon">
                                                <i class="icon-file-eye"></i>
                                            </a>
                                            @if(isset($item->team))
                                                <a href="#"
                                                   data-id="{{ $item->id }}"
                                                   title="Lihat Tim Penilai"
                                                   class="list-icons-item text-success btn-icon btn-create-team">
                                                    <i class="icon-users4"></i>
                                                </a>
                                            @else
                                                <a href="#"
                                                   data-id="{{ $item->id }}"
                                                   title="Tambah Tim Penilai"
                                                   class="list-icons-item text-danger btn-icon btn-create-team">
                                                    <i class="icon-users4"></i>
                                                </a>
                                            @endif
                                        </td>
                                        <td></td>
                                    </tr>
                                @endforeach--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.akuisisi.akuisisi.modal_team')
    @include('pages.akuisisi.akuisisi.modal_akuisisi')
@endsection

@push('addon-script')
    <script src="{{ asset('global_assets/js/plugins/forms/validation/validate.min.js') }}"></script>
    <script src="{{ asset('global_assets/plugins/sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    {{--    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>--}}
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
@endpush

@section('footer-script')
    <script src="{{ asset('global_assets/js/demo_pages/datatables_responsive.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>
    <script
        src="{{ asset('global_assets/plugins/gyrocode-datatables-checkboxes/js/dataTables.checkboxes.js') }}"></script>

    <script>
        let anggota = [
            {
                key: '',
            },
        ];

        let selected_rows = [];

        $(document).ready(function () {
            initSelect2();

            let table = $('.datatable-responsive-control').DataTable({
                processing: true,
                serverSide: false,
                ajax: '{{ route("akuisisi.get") }}',
                responsive: {
                    details: {
                        type: 'column',
                        target: -1
                    }
                },
                columns: [
                    {
                        data: 'id',
                        orderable: false,
                        searchable: false
                    },
                    {data: 'id'},
                    {data: 'klasifikasi.kode'},
                    {data: 'nama'},
                    {data: 'created_at'},
                    {data: 'berkas_kategori'},
                    {data: 'kurun'},
                    {
                        data: null,
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: null,
                        defaultContent: ""
                    },
                ],
                columnDefs: [
                    {
                        className: 'control',
                        orderable: false,
                        targets: -1
                    },
                    {
                        targets: 0,
                        checkboxes: {
                            selectRow: true
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            let checkbox = $(td).children('input[type="checkbox"]');

                            if (rowData.team && rowData.team.length > 0)
                                checkbox.prop('disabled', false);
                            else
                                checkbox.prop('disabled', true);
                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return moment(data).format("YYYY");
                        },
                    },
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return data.length;
                        },
                    },
                    {
                        targets: 7,
                        className: 'nowrap',
                        render: function (data, type, row) {
                            let html = '<a href="/akuisisi/' + row.id + '"\n' +
                                '   title="Lihat Detail"\n' +
                                '   class="list-icons-item text-info btn-icon">\n' +
                                '    <i class="icon-file-eye"></i>\n' +
                                '</a>';

                            if (row.team && row.team.length > 0)
                                html += '<a href="#"\n' +
                                    '   data-id="' + row.id + '"\n' +
                                    '   data-type="edit"\n' +
                                    '   title="Lihat Tim Penilai"\n' +
                                    '   class="list-icons-item text-success btn-icon btn-create-team">\n' +
                                    '    <i class="icon-users4"></i>\n' +
                                    '</a>';
                            else
                                html += '<a href="#"\n' +
                                    '   data-id="' + row.id + '"\n' +
                                    '   data-type="create"\n' +
                                    '   title="Tambah Tim Penilai"\n' +
                                    '   class="list-icons-item text-danger btn-icon btn-create-team">\n' +
                                    '    <i class="icon-users4"></i>\n' +
                                    '</a>';

                            return html;
                        },
                    },
                ],
                select: {
                    style: 'multi'
                },
            });

            table.on('order.dt search.dt', function () {
                table.column(1, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();

            table.on('change', 'input[type="checkbox"]', function (e) {
                $(this).parent().parent().toggleClass('selected');

                let rows_selected = table.column(0).checkboxes.selected();

                if (rows_selected.length > 0)
                    $('#btn-akuisisi-arsip').removeClass('d-none');
                else
                    $('#btn-akuisisi-arsip').addClass('d-none');
            });

            $('.dataTables_length select').select2({
                minimumResultsForSearch: Infinity,
                dropdownAutoWidth: true,
                width: 'auto'
            });

            buildTableAnggota(anggota);

            $('#btn-akuisisi-arsip').on('click', function () {
                let selected_rows = table.rows('.selected').data();
                let table_akuisisi = $('#table-akuisisi tbody');
                table_akuisisi.html('');

                $.each(selected_rows, function (key, value) {
                    table_akuisisi.append('<tr>\n' +
                        '    <td style="width: 10%;">' + (key + 1) + '<input type="hidden" name="berkas[]" value="' + value.id + '"></td>\n' +
                        '    <td>' + value.klasifikasi.kode + ' - ' + value.nama + '</td>\n' +
                        '</tr>');
                });

                $('#modal-akuisisi').modal({
                    show: true,
                    keyboard: false,
                    backdrop: 'static'
                });
            });

            $('#btn-cancel-save-akuisisi').on('click', function () {
                $('#modal-akuisisi').modal('hide');
            });

            $('.btn-add-anggota').on('click', function () {
                anggota.push({
                    key: '',
                });

                buildTableAnggota(anggota);
            });

            $('#btn-cancel-save').on('click', function () {
                anggota = [{
                    key: '',
                }];

                $('#modal-team').modal('hide');

                buildTableAnggota(anggota);
            });

            $('#btn-save').on('click', function (e) {
                anggota = [{
                    key: '',
                }];

                e.preventDefault();
                let form = $('#form-create-team');

                $.ajax({
                    method: 'POST',
                    dataType: 'json',
                    url: form.attr('action'),
                    data: form.serialize()
                }).done(function (response) {
                    form.find('.message').html(response.message);

                    if (response.status == true) {
                        Swal.fire(
                            'Berhasil!',
                            'Data berhasil disimpan!',
                            'success'
                        );

                        $('.datatable-responsive-control').DataTable().ajax.reload();

                        $('#modal-team').modal('hide');

                        buildTableAnggota(anggota);
                    }
                }).fail(function (response) {
                    form.find('.message').html(
                        '<div class="alert alert-warning" role="alert">Ada yang salah, silahkan muat ulang laman ini</div>'
                    );
                });
            });

            $('#btn-save-akuisisi').on('click', function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                let selected_rows = table.rows('.selected').data();
                let berkas = [];

                $.each(selected_rows, function (key, value) {
                    berkas.push({
                        'berkas_id': value.id,
                        'nama_berkas': value.nama,
                        'kurun': value.kurun,
                        'sifat': value.sifat,
                        'status_arsip': value.status_arsip,
                        'data': JSON.stringify(value),
                    });
                });

                let form = $('#form-create-akuisisi');

                $.ajax({
                    method: 'POST',
                    dataType: 'json',
                    url: form.attr('action'),
                    data: {
                        'berkas': berkas,
                        'penerima': $('#penerima').val(),
                        'kode_klasifikasi': $('#kode_klasifikasi').val(),
                    }
                }).done(function (response) {
                    form.find('.message').html(response.message);

                    if (response.status == true) {
                        Swal.fire(
                            // 'Berhasil!',
                            // 'Akuisisi Arsip dengan nomor Berita Acara <strong>' + response.data.bapp_code + '</strong> berhasil disimpan!',
                            // 'success'
                            {
                                title: 'Berhasil',
                                icon: 'success',
                                html: 'Akuisisi Arsip dengan nomor Berita Acara <strong>' + response.data.bapp_code + '</strong> berhasil disimpan!',
                            }
                        );

                        // location.reload();
                        $('.datatable-responsive-control').DataTable().ajax.reload();

                        $('#modal-akuisisi').modal('hide');
                    }
                }).fail(function (response) {
                    form.find('.message').html(
                        '<div class="alert alert-warning" role="alert">Ada yang salah, silahkan muat ulang laman ini</div>'
                    );
                });
            });
        });

        $(document).on('click', '.btn-create-team', function () {
            let type = $(this).data('type');
            let $tr = $(this).closest('tr');
            if ($tr.hasClass('child'))
                $tr = $($tr).prev('tr');

            let data = $('.datatable-responsive-control').DataTable().row($tr).data();

            let modalLabel = '';
            let formAction = '';
            if (type == 'create') {
                modalLabel = 'Tambah Tim Penilai';
                formAction = "{{ route('akuisisi.store') }}";
                anggota = [{
                    key: '',
                }];
            } else {
                modalLabel = 'Ubah Tim Penilai';
                formAction = '/akuisisi/update/' + data.id;
                if (data.team && data.team.length > 0) {
                    anggota = [];
                    data.team.forEach(function (value, key) {
                        if (value.role == 'anggota') {
                            anggota.push({
                                key: value.user_id
                            });
                        } else
                            $('#ketua').val(value.user_id).trigger('change');
                    });
                } else {
                    anggota = [{
                        key: '',
                    }];
                }
            }

            $('#modalLabel').html(modalLabel);
            let form = $('#form-create-team');
            form.attr('action', formAction);
            form.find('.message').html('');

            buildTableAnggota(anggota);

            $('#berkas_id').val(data.id);
            $('#modal-team').modal({
                show: true,
                keyboard: false,
                backdrop: 'static'
            });
        });

        $(document).on('click', '.btn-remove-anggota', function () {
            let key = $(this).data('key');
            anggota.splice(key, 1);

            buildTableAnggota(anggota);
        });

        $(document).on('change', '.select-anggota', function () {
            let value = $(this).val();
            let key = $(this).data('id');

            anggota[key].key = value;
        });

        function initSelect2() {
            $('.form-control-select2').select2();
        }

        function buildTableAnggota(data) {
            let table_body = $('#table-anggota tbody');
            let html_rows = '';

            data.forEach(function (value, key) {
                let index = key + 1;
                html_rows += '<tr>\n' +
                    '                                    <td>' + index + '</td>\n' +
                    '                                    <td style="width: 80%;" class="pr-0">\n' +
                    '                                        <div class="form-row">\n' +
                    '                                            <div class="form-group col-12 my-0">\n' +
                    '                                                <select name="anggota[]"\n' +
                    '                                                        data-id="' + key + '"\n' +
                    '                                                        id="anggota' + index + '"\n' +
                    '                                                        class="form-control form-control-select2 select-anggota"\n' +
                    '                                                        data-placeholder="Pilih Anggota"\n' +
                    '                                                        required>\n' +
                    '                                                    <option value=""></option>\n' +
                    '                                                    <option value="1">Anggota 1</option>\n' +
                    '                                                    <option value="2">Anggota 2</option>\n' +
                    '                                                </select>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                    </td>\n' +
                    '                                    <td class="text-right">\n' +
                    '                                        <button type="button"\n' +
                    '                                                class="btn btn-outline-danger btn-sm btn-remove-anggota"\n' +
                    '                                                data-key="' + key + '"\n' +
                    '                                                title="Hapus Anggota">\n' +
                    '                                            <i class="icon-trash"></i>\n' +
                    '                                        </button>\n' +
                    '                                    </td>\n' +
                    '                                </tr>';
            });

            table_body.html(html_rows);
            // initSelect2();

            data.forEach(function (value, key) {
                let index = key + 1;
                let select = $('#anggota' + index).select2();

                $(select).val(value.key).trigger('change');
            });
        }
    </script>
@endsection
