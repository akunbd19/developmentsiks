@extends('layouts.begin_back')

@section('daftar_arsip', 'active')

@section('title', 'Detail Arsip')

@section('content')

<!-- Page header -->
<div class="page-header-content header-elements-md-inline">
    <div class="page-title d-flex">
        <h4><i class="icon-folder-search mr-2"></i> <span class="font-weight-semibold">Isi Arsip</span>
        </h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>

    <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
        <a href="{{ route('arsip.index') }}" class="btn btn-labeled btn-labeled-left bg-grey"><b><i
                    class="icon-arrow-left8"></i></b>Kembali</a>
    </div>
</div>
<!-- /page header -->

<div class="page-content pt-0">
    <div class="content-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Informasi Arsip 1 - ANRI</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-2">Klasifikasi</div>
                                <div class="col-sm-10">: IK.02 - Pengelolaan Data dan Informasi SIKN dan JIKN</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">Retensi Aktif</div>
                                <div class="col-sm-10">: 3</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">Retensi Inaktif</div>
                                <div class="col-sm-10">: 2</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">Penyusutan Akhir</div>
                                <div class="col-sm-10">: Permanen</div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6">Tanggal Tutup Berkas</div>
                                <div class="col-sm-6">Uraian</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    2021-09-07
                                </div>
                                <div class="col-sm-6">
                                    -
                                </div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-sm-6">Lokasi Fisik</div>
                                <div class="col-sm-3">Latitude</div>
                                <div class="col-sm-3">Longitude</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    -
                                </div>
                                <div class="col-sm-3">
                                    -
                                </div>
                                <div class="col-sm-3">
                                    -
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <span>Dibuat pada : 7 September 2021</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Daftar Isi Berkas Aktif</h5>
                        </div>
                        <table class="table datatable-responsive-control-right table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Jenis Naskah</th>
                                    <th>Nomor Naskah</th>
                                    <th>Tanggal</th>
                                    <th>Hal</th>
                                    <th>File</th>
                                    <th width="10%" class="text-center">Aksi</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>MASUk</td>
                                    <td>DI.01.02/1/2021</td>
                                    <td>02-09-2021</td>
                                    <td>Uji Coba SRIKANDI Versi 2</td>
                                    <td>
                                        <a target="_blank" href="#"
                                            class="btn bg-pink btn-sm"><i class="icon-file-pdf pr-1"></i> Lihat</a>
                                    </td>
                                    <td class="text-danger text-center">
                                        <div class="list-icons">
                                            <a href="#"
                                                class="list-icons-item text-secondary btn-icon"><i
                                                    class="icon-eye"></i></a>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('addon-script')
<script src="{{ asset('global_assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
@endpush

@section('footer-script')
<script src="{{ asset('global_assets/js/demo_pages/datatables_responsive.js') }}"></script>
@endsection