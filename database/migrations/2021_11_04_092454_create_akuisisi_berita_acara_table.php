<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAkuisisiBeritaAcaraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akuisisi_berita_acara', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_klasifikasi');
            $table->string('penerima_id');
            $table->string('nama_penerima');
            $table->string('nomor');
            $table->string('file_name');
            $table->string('file_path');
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akuisisi_berita_acara');
    }
}
