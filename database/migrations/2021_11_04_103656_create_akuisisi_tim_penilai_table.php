<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAkuisisiTimPenilaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akuisisi_tim_penilai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('berkas_id');
            $table->bigInteger('user_id');
            $table->string('role')->default('anggota');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akuisisi_tim_penilai');
    }
}
